'use strict';
/* 
 Created on : 27 Nov, 2015, 10:04:33 PM
 Author     : Amit 
 */

/* This is a  node.js server file 
 * Requires node installation on system.
 * Requires npm package manager to fetch and install dependicies.
 * Run npm install in the root directory where this file is located and pacakge.json is located.
 * Then run node index(filename of server) in terminal.
 * And thats it ...! your server is running on localhost:3000
 */
var express = require("express");
var multer = require('multer');
var app = express();
var util = require('util');
var walk = require('walk');

var path = require('path');
var fs = require('fs');
var os = require("os");
var bodyParser = require('body-parser');
var unoconv = require('unoconv');
var spawn = require('child_process').spawn;

//GLOBAL APP DECLARATION
var doneDir = path.normalize('./app/steno_complete/');
var editDir = path.normalize('./app/steno_edit/');
var newDir = path.normalize('./app/steno_done/');
var dataDir = path.normalize('./app/steno_data/');
var templateDir = path.normalize('./app/steno_templates/');


var done = multer({dest: doneDir, rename: function (fieldname, filename) {
        return filename.split('(')[0] + '(' + Date.now() + ')';
    }});
var edit = multer({dest: editDir, rename: function (fieldname, filename) {
        return filename.split('(')[0] + '(' + Date.now() + ')';
    }});

//APP CONFIGURATION 
app.use(express.static('app')); // use this as resource  directory
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//APP ROUTING URL => FUNCTIONS
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/app/index.html");
});
app.get('/editor', function (req, res) {
    res.sendFile(__dirname + "/app/editor.html");
});
app.get('/get-new-files', function (req, res) {
    var files = [];
    var walker = walk.walk('./app/steno_done', {followLinks: false});
    walker.on('file', function (root, stat, next) {
        // Add this file to the list of files


        files.push(root + '/' + stat.name);
        next();
    });

    walker.on('end', function () {
        console.log(files);
        res.json({files: files});
    });

});
app.get('/get-edited-files', function (req, res) {
    var files = [], sortFiles = [];
    var walker = walk.walk('./app/steno_edit', {followLinks: false});
    walker.on('file', function (root, stat, next) {
        // Add this file to the list of files
        files.push(root + '/' + stat.name);
        files.sort(function (a, b) {
            return fs.statSync(b).mtime.getTime() -
                    fs.statSync(a).mtime.getTime();
        });

        next();
    });

    walker.on('end', function () {
        console.log(files);
        res.json({files: files});
    });

});
app.get('/get-history', function (req, res) {
    var files = [];
    var walker = walk.walk('./app/steno_history', {followLinks: false});
    walker.on('file', function (root, stat, next) {
        // Add this file to the list of files
        files.push(root + '/' + stat.name);
        files.sort(function (a, b) {
            return fs.statSync(b).mtime.getTime() -
                    fs.statSync(a).mtime.getTime();
        });
        next();
    });


    walker.on('end', function () {
        console.log(files);
        res.json({files: files});
    });

});
app.post('/sign-send', done, function (req, res) {

    console.log("Sign and Send service executed.");
    var filesUploaded = 0;
    
    if (Object.keys(req.files).length === 0) {
        console.log('no files uploaded');
    } else {
        console.log(req.files);
        var files = req.files.file1;
        if (!util.isArray(req.files.file1)) {
            files = [req.files.file1];
        }
        filesUploaded = files.length;
        var soffice = spawn('soffice', ['--headless', '--convert-to', 'pdf', '--outdir', './app/steno_history', req.files.userPhoto.path]);
        soffice.stdout.on('data', function (data) {
            console.log('stdout: ' + data);
        });

        soffice.stderr.on('data', function (data) {
            console.log('stderr: ' + data);
        });

        soffice.on('close', function (code) {
            console.log('child process exited with code ' + code);
            if (fileExists(newDir + req.files.userPhoto.originalname)) {
                fs.unlinkSync(newDir + req.files.userPhoto.originalname);
            } else {
                if (fileExists(editDir + req.files.userPhoto.originalname)) {
                    fs.unlinkSync(editDir + req.files.userPhoto.originalname);
                } else {
                    console.log(req.files.userPhoto.originalname + " Not Found. Please Ensure file was not deleted or moved manually.");
                }
            }
            res.json({message: 'Finished! Uploaded ' + filesUploaded + ' files.  Route is /files1', date: new Date(), file: req.files.userPhoto});
        });

    }


});

app.post('/edit', edit, function (req, res) {

    console.log("Edit or Save document service executed.");

    var filesUploaded = 0;

    if (Object.keys(req.files).length === 0) {
        console.log('no files uploaded');
    } else {
        console.log(req.files);

        var files = req.files.file1;
        if (!util.isArray(req.files.file1)) {
            files = [req.files.file1];
        }

        filesUploaded = files.length;
    }
    if (fileExists(newDir + req.files.userPhoto.originalname)) {
        fs.unlinkSync(newDir + req.files.userPhoto.originalname);
    } else {
        if (fileExists(editDir + req.files.userPhoto.originalname)) {
            fs.unlinkSync(editDir + req.files.userPhoto.originalname);
        } else {
            console.log(req.files.userPhoto.originalname + " Not Found. Please Ensure file was not deleted or moved manually.");
        }
    }

//    fs.unlinkSync(newDir + req.files.userPhoto.originalname, function (err) {
//        if (err) { // if file not found in New folder
//            fs.unlinkSync(editDir + req.files.userPhoto.originalname, function (err) {
//                if (err) {//if file not found in edit folder
//                    console.log(req.files.userPhoto.originalname + " Not Found. Please Ensure file was not deleted or moved manually.");
//                }
//            });
//        }
//    });

    res.json({message: 'Finished! Uploaded ' + filesUploaded + ' files.  Route is /files1', date: new Date(), file: req.files.userPhoto});
});

app.post('/save-form', done, function (req, res) {
    console.log("Form service executed.");
    console.log("Id is " + req.body.id);
    var fileDir = dataDir + 'steno_data.txt';
    fs.appendFile(fileDir, JSON.stringify(req.body) + '\r\n', function () {
        res.end();
    });
});

app.get('/get-form', function (req, res) {
    var fileDir = dataDir + 'steno_data.txt';
    fs.readFile(fileDir, 'utf8', function (err, data) {
        if (err)
            throw err;
//        console.log("GET "+data);
    });
});

//get sample templates
app.get('/get-templates', function (req, res) {
    var fileDir = templateDir + 'steno_autotext.txt';
    fs.readFile(fileDir, 'utf8', function (err, data) {
        if (err)
            throw err;
        res.send(data);
    });
});

//START THE SERVER
app.listen(process.env.PORT || 3000, "0.0.0.0", function () {
    console.log("Document-Editor has started succesfully and is working on http://localhost:3000");
});

function fileExists(filePath)
{
    try
    {
        return fs.statSync(filePath).isFile();
    } catch (err)
    {
        return false;
    }
}
