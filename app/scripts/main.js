
var _editor_global, _loadedFilename_global;
var _baseUrl = "";
var sign = function () {
    function saveByteArrayLocally(err, data) {
        if (err) {
            alert(err);
            return;
        }
        var mimetype = "application/vnd.oasis.opendocument.text";
        var filename = _loadedFilename_global || "doc.odt";
        var blob = new Blob([data.buffer], {type: mimetype});
        var _data = new FormData();
        _data.append("userPhoto", blob, filename);
        var xhr = new XMLHttpRequest();
        xhr.open("post", _baseUrl + "/sign-send", true);
        xhr.onload = function (e) {
            if (this.status === 200) {
                $('#saveModal').modal('show');
            }
        };
        xhr.send(_data);

        _editor_global.setDocumentModified(false);
    }
    _editor_global.getDocumentAsByteArray(saveByteArrayLocally);
};


var saveAsDraft = function () {
    function saveByteArrayLocally(err, data) {
        if (err) {
            alert(err);
            return;
        }
        var mimetype = "application/vnd.oasis.opendocument.text";
        var filename = _loadedFilename_global || "doc.odt";
        var blob = new Blob([data.buffer], {type: mimetype});
        var _data = new FormData();
        _data.append("userPhoto", blob, filename);
        var xhr = new XMLHttpRequest();
        xhr.open("post", _baseUrl + "/edit", true);
        xhr.onload = function (e) {
            if (this.status === 200) {
                $('#saveModal').modal('show');
            }
        };
        xhr.send(_data);

        _editor_global.setDocumentModified(false);
    }
    _editor_global.getDocumentAsByteArray(saveByteArrayLocally);
};


$('#quick-button').tooltip();
var down = [];
var selectedKey;
$(document).keydown(function (e) {
    down[e.keyCode] = true;
}).keyup(function (e) {
    if (down[115]) {
        $('#selectTemplate').dropdown('toggle');
        $('#selectTemplate').find('#1>a').focus();
        if ((down[49] || down[50] || down[51] || down[52] || down[53] || down[54] || down[55] || down[56] || down[57] || down[48] || down[96] || down[97] || down[98] || down[99] || down[100] || down[101] || down[102] || down[103] || down[104] || down[105])) {

            if (e.keyCode >= 48 && e.keyCode <= 57) {
                selectedKey = e.keyCode - 48;
            } else {
                selectedKey = e.keyCode - 96;
            }

            $('#selectTemplate').find('#' + selectedKey + '>a').focus();
            console.log(e.keyCode);
        }
    }
    down[e.keyCode] = false;
});
$("#showForm").click(function () {
    $("#patientForm").slideToggle("slow");
});
$("#dob").datepicker({dateFormat: 'dd-mm-yy'});
var disableButton = function () {
    if ($('input[name=format]:checked').val() === "inline" || $('input[name=format]:checked').val() === "table") {
        $("#formSubmit").attr('disabled', true);
    }
};
var submitForm = function () {
    var formElement = document.querySelector("form");
    var displayElement = $('#patientDetailsForm').serialize();
    var displayForm = displayElement.split("&").join(", ");

    var formData = new FormData(formElement);
    var randomId = generateUUID();
    formData.append("id", randomId);
    var http = new XMLHttpRequest();
    http.open("post", _baseUrl + "/save-form", true);
    http.onload = function (e) {
        if (this.status === 200) {
//            $('#saveForm').modal('show');
            $("#patientForm").hide();
            $("#formSubmit").attr('disabled', true);
            if ($('input[name=format]:checked').val() === "inline") {
                _editor_global.insertText(displayForm);
            } else if ($('input[name=format]:checked').val() === "table") {
                var formArray = $('#patientDetailsForm').serializeArray();
                for (var i = 0; i < formArray.length; i++) {
                    var tmp = formArray[i];
                    formArray[i] = [tmp.name, tmp.value];
                }
                _editor_global.insertTable(formArray);
            }
        }
    };
    http.send(formData);
    return false;
};

var getForm = function () {
    var http = new XMLHttpRequest();
    http.open("get", _baseUrl + "/get-form?" + Math.random(), true);
    http.send();
};
getForm();
var getTemplates = function () {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            var myArr = JSON.parse(http.responseText);
            console.log(myArr);
            populateList(myArr);
        }
    };
    http.open("get", _baseUrl + "/get-templates?" + Math.random(), true);
    http.send();
    console.log(http.responseText);
};
getTemplates();

var populateList = function (arr) {
    var option = '';
    for (var i = 0; i < arr.length; i++) {
        var id = i + 1;
        option += '<li id=' + id + '><a href="#" >' + arr[i].name + '<div class="description">' + arr[i].description + '</div></a></li>';
    }
    $('#selectTemplate').append(option);
    $("#selectTemplate > li").on('click', function () {
        var target = $(this).find('div')[0].innerHTML;
        _editor_global.insertText(target);
    });

};
var putTemplateText = function (arr) {

};
function generateUUID() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now();
        ; //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}
;
function createEditor(fileName) {
    "use strict";

    var editor = null,
            editorOptions,
            loadedFilename;

    /*jslint emptyblock: true*/
    /**
     * @return {undefined}
     */
    function startEditing() {
//        $('.doc-editor-wrapper').attr("contenteditable", "true");
//        $('.doc-editor-wrapper').attr("readonly", "true");

        var $webodfeditor = $('#editorContainer') ;
        var $container = $webodfeditor.find("document").parent();
        $container.attr("contenteditable", "true");
        $container.attr("readonly", "true");
    }
    /*jslint emptyblock: false*/

    /**
     * extract document url from the url-fragment
     *
     * @return {?string}
     */
    function guessDocUrl() {
        var pos, docUrl = String(document.location);
        // If the URL has a fragment (#...), try to load the file it represents
        pos = docUrl.indexOf('#');
        if (pos !== -1) {
            docUrl = docUrl.substr(pos + 1);
        } else {
            docUrl = fileName;
        }
        return docUrl || null;
    }

    function fileSelectHandler(evt) {
        var file, files, reader;
        files = (evt.target && evt.target.files) ||
                (evt.dataTransfer && evt.dataTransfer.files);
        function onLoadEnd() {
            if (reader.readyState === 2) {
                runtime.registerFile(file.name, reader.result);
                loadedFilename = file.name;
                editor.openDocumentFromUrl(loadedFilename, startEditing);
            }
        }
        if (files && files.length === 1) {
            if (!editor.isDocumentModified() ||
                    window.confirm("There are unsaved changes to the file. Do you want to discard them?")) {
                editor.closeDocument(function () {
                    file = files[0];
                    reader = new FileReader();
                    reader.onloadend = onLoadEnd;
                    reader.readAsArrayBuffer(file);
                });
            }
        } else {
            alert("File could not be opened in this browser.");
        }
    }

    function enhanceRuntime() {
        var openedFiles = {},
                readFile = runtime.readFile;
        runtime.readFile = function (path, encoding, callback) {
            var array;
            if (openedFiles.hasOwnProperty(path)) {
                array = new Uint8Array(openedFiles[path]);
                callback(undefined, array);
            } else {
                return readFile(path, encoding, callback);
            }
        };
        runtime.registerFile = function (path, data) {
            openedFiles[path] = data;
        };
    }

    function createFileLoadForm() {
        var form = document.createElement("form"),
                input = document.createElement("input");

        function internalHandler(evt) {
            if (input.value !== "") {
                fileSelectHandler(evt);
            }
            // reset to "", so selecting the same file next time still trigger the change handler
            input.value = "";
        }
        form.appendChild(input);
        form.style.display = "none";
        input.id = "fileloader";
        input.setAttribute("type", "file");
        input.addEventListener("change", internalHandler, false);
        document.body.appendChild(form);
    }

    function load() {
        var form = document.getElementById("fileloader");
        if (!form) {
            enhanceRuntime();
            createFileLoadForm();
            form = document.getElementById("fileloader");
        }
        form.click();
    }

    function save() {
        function saveByteArrayLocally(err, data) {
            if (err) {
                alert(err);
                return;
            }
            var mimetype = "application/vnd.oasis.opendocument.text";
            var filename = loadedFilename || "doc.odt";
            var blob = new Blob([data.buffer], {type: mimetype});
            var _data = new FormData();
            _data.append("userPhoto", blob, filename);
            var xhr = new XMLHttpRequest();
            xhr.open("post", _baseUrl + "/edit", true);
            xhr.onload = function (e) {
                if (this.status === 200) {
                    $('#saveModal').modal('show');
                }
            };
            xhr.send(_data);

            editor.setDocumentModified(false);
        }

        editor.getDocumentAsByteArray(saveByteArrayLocally);
    }

    editorOptions = {
        loadCallback: load,
        saveCallback: save,
        allFeaturesEnabled: true
    };

    function onEditorCreated(err, e) {
        var docUrl = guessDocUrl();
        if (fileName.indexOf('steno_history') >= 0) {
            $('.dijitToolbar').css("display", "none");
        }

        if (err) {
            // something failed unexpectedly
            alert(err);
            return;
        }

        editor = e;
        _editor_global = e;
        editor.setUserData({
            fullName: "WebODF-Curious",
            color: "black"
        });

        window.addEventListener("beforeunload", function (e) {
            var confirmationMessage = "There are unsaved changes to the file.";

            if (editor.isDocumentModified()) {
                // Gecko + IE
                (e || window.event).returnValue = confirmationMessage;
                // Webkit, Safari, Chrome etc.
                return confirmationMessage;
            }
        });

        if (docUrl) {
            loadedFilename = docUrl;
            _loadedFilename_global = loadedFilename;
            editor.openDocumentFromUrl(docUrl, startEditing);
        }
    }

    Wodo.createTextEditor('editorContainer', editorOptions, onEditorCreated);
}
